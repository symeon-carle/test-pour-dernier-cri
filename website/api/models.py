from django.db import models


class Article(models.Model):
    title = models.TextField()
    summary = models.TextField()
    text = models.TextField()
    thumbnail = models.URLField(max_length=500)
    image = models.URLField(max_length=500)
