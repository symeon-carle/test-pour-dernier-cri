from django.urls import include, path
from rest_framework.routers import DefaultRouter
from rest_framework.schemas import get_schema_view

from . import apps, views

router = DefaultRouter()
router.register("articles", views.ArticleViewSet)

app_name = apps.ApiConfig.name
urlpatterns = [
    path("", include(router.urls)),
    path(
        "openapi",
        get_schema_view(
            title="Articles API",
            description="API that stores articles",
            version="1.0.0",
        ),
        name="openapi-schema",
    ),
]
