import requests
from bs4 import BeautifulSoup, Tag
from django.core.management.base import BaseCommand

from api.models import Article


class Command(BaseCommand):
    help = "Push some articles from Le Monde's RSS feed"

    def handle(self, *args, **options):
        rss_req = requests.get("https://www.lemonde.fr/rss/une.xml")
        rss_req.raise_for_status()
        rss_feed = BeautifulSoup(rss_req.text, "html.parser")
        for item in rss_feed.find("channel").find_all("item"):
            title = item.title.string or ""
            description = item.description.string or ""
            image = item.find("media:content")["url"] or ""
            url = item.guid.string
            article_response = requests.get(url)
            article_html = BeautifulSoup(article_response.text, "html.parser")
            content_element = (
                article_html.find("article")
                or article_html.find("section", id="post-container")
            )
            if content_element is None:
                print("Couldn't parse content from webpage, skipping ...")
                continue
            
            text = content_element.text
            article = Article(
                title=title,
                summary=description,
                text=text,
                thumbnail=image,
                image=image,
            )
            article.save()
            print(f"Saved article {title!r}")
