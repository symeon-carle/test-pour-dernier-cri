from django.views import generic

from api.models import Article


class ArticleListView(generic.ListView):
    model = Article
    template_name = "front/article_list.html"
    context_object_name = "articles"

    def get_queryset(self):
        return Article.objects.all()


class ArticleView(generic.DetailView):
    model = Article
    template_name = "front/article_detail.html"
