from django.urls import include, path

from . import apps, views

app_name = apps.FrontConfig.name
urlpatterns = [
    path("", views.ArticleListView.as_view(), name="home"),
    path("<int:pk>/", views.ArticleView.as_view(), name="article"),
]
