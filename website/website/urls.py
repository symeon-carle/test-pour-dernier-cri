from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.http import HttpResponseRedirect
from django.urls import include, path, reverse

urlpatterns = [
    path("", lambda r: HttpResponseRedirect(reverse("front:home"))),
    path("front/", include("front.urls")),
    path("api/", include("api.urls")),
    path("admin/", admin.site.urls),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
