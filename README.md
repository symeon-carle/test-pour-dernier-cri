# Test technique pour Dernier Cri
## Installation
Le project utilise [poetry](https://python-poetry.org/) pour gérer ses dépendances.

- Cloner le projet <br> `$ git clone git@gitlab.com:symeon-carle/test-pour-dernier-cri.git`
- Aller à la racine du repo <br> `$ cd test-pour-dernier-cri`
- Installer poetry <br> `$ curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python -`
- Lancer un shell dans le virtualenv <br> `$ poetry shell`
- Installer les dépendances <br> `$ poetry install`
- Aller dans le dossier `website` <br> `$ cd website`
- Créer la BDD de test <br> `$ ./manage.py migrate`
- Remplir la BDD <br> `$ ./manage.py populate_db`
- Lancer le serveur de test <br> `$ ./manage.py runserver`

Visiter l'url indiquée avec un navigateur, on arrive normalement sur la liste
des articles.

## Détails du Back
Pour faire vite j'ai choisi Django Rest Framework

Un seul modèle simple

Pour les images je stocke des url, pas des blobs :
- beaucoup plus facile à intégrer au front
- on peux déléguer l'hosting à qui on veut.
- en ce moment j'évite la gestion de blobs dans une base relationnelle,
  je trouve que les cas où ils sont un bon compromis se font de plus en plus
  rares.

Dans les faits le thumbnail et l'image complète sont les mêmes, le flux du
Monde ne donne qu'une image, je n'ai pas essayé de faire compliqué.

J'ai retiré l'authentification, c'est juste un test après tout.

J'ai ajouté une route qui sert la spec OpenAPI, je voulais m'en servir pour
faire un front plus clairement séparé mais je ne suis pas allé jusque là.

## Détails du Front
J'ai un peu manqué de temps par rapport à ce que je pensais faire, alors je
me suis rabattu sur un front rendu côté serveur tout simple avec un poil
de style.

## Points d'amélioration
### Mettre une vraie DB
C'est surement le premier truc à faire si on passe à un vrai projet, le code
actuel se repose sur le fichier sqlite local par défaut.

### Faire un front complètement séparé
Je m'y connais moins en front, mais ce que je sais en revanche c'est que le
rendu côté serveur c'est à reserver pour les toutes petites charges.

Je n'ai pas vraiment d'avis niveau technologies pour un projet javascript.
Mon seul grain de sel pour ça ça serai de regader ce qui existe en génération
de code client à partir d'une spec OpenAPI. Préférenciellement vers un language
transpilable en javascript qui soit aussi un minimum typé genre TypeScript ou
Elm. Le tout pour pouvoir avoir une erreur à la *compilation* plutôt qu'à
l'execution si le code front n'est plus en phase avec l'api.

### Du style
je suis pas bon en css mais pas besoin d'être un expert pour voir qu'on peux
faire mieux que ce que j'ai fait en vitesse.

Aussi il faut configuer une solution de production pour *le* fichier statique

### Des tests
Dans cette micro application il n'y encore aucune logique métier, donc en
essence pour l'instant rien à tester si ce n'est s'assurer que django rest
framework lui même fonctionne.

### Linters et formatteurs
Je déteste devoir faire la moindre action de formattage à la main. Je ne l'ai
pas fait ici mais même pour les petits projets j'aime bien avoir un set
d'outils qui font un maximum de nettoyage automatiquement.

### Du typage
Django ne facilite pas l'utilisation efficace de mypy avec sa conception à
base de classes qui servent juste de namespaces, mais n'importe quel base de
code python de plus d'un fichier bénéficierai selon moi d'utiliser mypy.

### De l'auth
J'ai retiré presque tout pour ce test mais évidemment une application en prod
va en général avoir besoin d'une gestion beaucoup plus complète que ça.

### Permettre de stocker du contenu en markdown dans le corps des articles
Du texte brut c'est vite ennuyeux. Mais bon attention à ne pas laisser passer
du html arbitraire.
